package s520857.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;

import com.backendless.Backendless;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.DataQueryBuilder;

public class MainActivity extends AppCompatActivity {

    // Setting up queries
    DataQueryBuilder q1 = DataQueryBuilder.create();
    DataQueryBuilder q2 = DataQueryBuilder.create();
    DataQueryBuilder q3 = DataQueryBuilder.create();
    DataQueryBuilder q4 = DataQueryBuilder.create();
    DataQueryBuilder q5 = DataQueryBuilder.create();
    DataQueryBuilder q6 = DataQueryBuilder.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setting up backendless
        Backendless.setUrl(Defaults.SERVER_URL);
        Backendless.initApp(getApplicationContext(), Defaults.APPLICATION_ID, Defaults.API_KEY);

        q1.setWhereClause("typeofEncounter = 'Crash'");
        q2.setWhereClause("typeofEncounter = 'Warning'");
        q3.setWhereClause("typeofEncounter = 'Ticket'");
        q4.setWhereClause("typeofEncounter = 'Other'");
        q5.setWhereClause("policeType = 'City / County Police'");
        q6.setWhereClause("policeType = 'State Trooper'");

        // Initializing Text Views
        TextView crashTV = (TextView) findViewById(R.id.crashTV);
        TextView warningTV = (TextView) findViewById(R.id.warningTV);
        TextView ticketTV = (TextView) findViewById(R.id.ticketTV);
        TextView otherTV = (TextView) findViewById(R.id.otherTV);
        TextView localPoliceTV = (TextView) findViewById(R.id.localPoliceTV);
        TextView statePoliceTV = (TextView) findViewById(R.id.statePoliceTV);

        getStuff(q1, crashTV, "Crash");
        getStuff(q2, warningTV, "Warning");
        getStuff(q3, ticketTV, "Ticket");
        getStuff(q4, otherTV, "Other");
        getStuff(q5, localPoliceTV, "Local Police");
        getStuff(q6, statePoliceTV, "State Police");

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        q1.setWhereClause("typeofEncounter = 'Crash'");
        q2.setWhereClause("typeofEncounter = 'Warning'");
        q3.setWhereClause("typeofEncounter = 'Ticket'");
        q4.setWhereClause("typeofEncounter = 'Other'");
        q5.setWhereClause("policeType = 'City / County Police'");
        q6.setWhereClause("policeType = 'State Trooper'");

        // Initializing Text Views
        TextView crashTV = (TextView) findViewById(R.id.crashTV);
        TextView warningTV = (TextView) findViewById(R.id.warningTV);
        TextView ticketTV = (TextView) findViewById(R.id.ticketTV);
        TextView otherTV = (TextView) findViewById(R.id.otherTV);
        TextView localPoliceTV = (TextView) findViewById(R.id.localPoliceTV);
        TextView statePoliceTV = (TextView) findViewById(R.id.statePoliceTV);

        getStuff(q1, crashTV, "Crash");
        getStuff(q2, warningTV, "Warning");
        getStuff(q3, ticketTV, "Ticket");
        getStuff(q4, otherTV, "Other");
        getStuff(q5, localPoliceTV, "Local Police");
        getStuff(q6, statePoliceTV, "State Police");
    }

    public void addLocation(View v){
        Intent ini = new Intent(this,AddLocation.class);
        startActivity(ini);
    }

    public void test(View v){
        Intent ini = new Intent(this, MapsActivity.class);
        startActivity(ini);
    }

    public void getStuff(DataQueryBuilder q, final TextView t, final String type) {

        Backendless.Data.of( "Locations" ).getObjectCount( q, new AsyncCallback<Integer>() {

                    @Override
                    public void handleResponse( Integer integer ) {
                        t.setText(type + "\n" + integer);
                    }

                    @Override
                    public void handleFault( BackendlessFault backendlessFault ) {
                        Log.i( "MYAPP" , "error - " + backendlessFault.getMessage());
                    }
        } );
    }
}
